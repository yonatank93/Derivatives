import math


class DualNumber:
    """Dual number :math:`y = a + b \\cdot \\epsilon`. The following operations
    are modification from
    https://code.activestate.com/recipes/580610-auto-differentiation/.

    Parameters
    ----------
    val, der: float
        Coefficient of the dual number.
    """

    def __init__(self, val, der=0):
        self.val = val
        self.der = der

    def __add__(self, y2):
        return DualNumber(self.val + V(y2), self.der + D(y2))

    def __sub__(self, y2):
        return DualNumber(self.val - V(y2), self.der - D(y2))

    def __mul__(self, y2):
        u, v, du, dv = self.val, V(y2), self.der, D(y2)
        return DualNumber(u * v, (du * v) + (u * dv))

    def __truediv__(self, y2):
        u, v, du, dv = self.val, V(y2), self.der, D(y2)
        return DualNumber(u / v, (du * v - u * dv) / v ** 2)

    def __pow__(self, y2):
        u, v, du, dv = self.val, V(y2), self.der, D(y2)
        return DualNumber(
            u ** v,
            (u ** (v - 1) * (du * v + math.log(abs(u)) * u * dv)),
        )

    def __floordiv__(self, y2):
        return DualNumber(self.val // V(y2), 0)

    def __mod__(self, y2):
        u, v, du, dv = self.val, V(y2), self.der, D(y2)
        return DualNumber(u % v, du - u // v * dv)

    def __pos__(self):
        return self

    def __neg__(self):
        return DualNumber(-self.val, -self.der)

    __radd__ = __add__
    __rmul__ = __mul__

    def __rsub__(self, y2):
        return DualNumber(y2) - self

    def __rtruediv__(self, y2):
        return DualNumber(y2) / self

    def __rpow__(self, y2):
        return DualNumber(y2) ** self

    def __rmod__(self, y2):
        return DualNumber(y2) % self

    def __rfloordiv__(self, y2):
        return DualNumber(y2) // self

    def __abs__(self):
        return DualNumber(
            self.val if self.val >= 0 else -self.val,
            self.der if self.der >= 0 else -self.der,
        )

    def sqrt(self):
        return DualNumber(
            math.sqrt(self.val), self.der / (2 * math.sqrt(self.val))
        )

    def log(self):
        return DualNumber(math.log(self.val), self.der / self.val)

    def log2(self):
        return DualNumber(
            math.log2(self.val), self.der / (self.val * math.log(2))
        )

    def log10(self):
        return DualNumber(
            math.log10(self.val), self.der / (self.val * math.log(10))
        )

    def log1p(self):
        return DualNumber(math.log1p(self.val), self.der / (self.val + 1))

    def exp(self):
        return DualNumber(math.exp(self.val), math.exp(self.val) * self.der)

    def expm1(self):
        return DualNumber(math.expm1(self.val), math.exp(self.val) * self.der)

    def sin(self):
        return DualNumber(math.sin(self.val), math.cos(self.val) * self.der)

    def cos(self):
        return DualNumber(math.cos(self.val), -math.sin(self.val) * self.der)

    def tan(self):
        return DualNumber(
            math.tan(self.val), self.der / math.cos(self.val) ** 2
        )

    def arcsin(self):
        return DualNumber(
            math.asin(self.val), self.der / math.sqrt(1 - self.val ** 2)
        )

    def arccos(self):
        return DualNumber(
            math.acos(self.val), -self.der / math.sqrt(1 - self.val ** 2)
        )

    def arctan(self):
        return DualNumber(math.atan(self.val), self.der / (1 + self.val ** 2))

    def sinh(self):
        return DualNumber(math.sinh(self.val), math.cosh(self.val) * self.der)

    def cosh(self):
        return DualNumber(math.cosh(self.val), math.sinh(self.val) * self.der)

    def tanh(self):
        return DualNumber(
            math.tanh(self.val), self.der / math.cosh(self.val) ** 2
        )

    def arcsinh(self):
        return DualNumber(
            math.asinh(self.val), self.der / math.sqrt(self.val ** 2 + 1)
        )

    def arccosh(self):
        return DualNumber(
            math.acosh(self.val), self.der / math.sqrt(self.val ** 2 - 1)
        )

    def arctanh(self):
        return DualNumber(math.atanh(self.val), self.der / (1 - self.val ** 2))

    def radians(self):
        return DualNumber(math.radians(self.val), math.radians(self.der))

    def degrees(self):
        return DualNumber(math.degrees(self.val), math.degrees(self.der))

    def hypot(self, y2):
        u, v, du, dv = self.val, V(y2), self.der, D(y2)
        dist = math.hypot(u, v)
        return DualNumber(dist, (u * du + v * dv) / dist)

    def erf(self):
        return DualNumber(
            math.erf(self.val),
            2 / math.sqrt(math.pi) * math.exp(-(self.val ** 2)) * self.der,
        )

    def erfc(self):
        return DualNumber(
            math.erfc(self.val),
            -2 / math.sqrt(math.pi) * math.exp(-(self.val ** 2)) * self.der,
        )

    def gamma(self):
        raise NotImplementedError

    def lgamma(self):
        raise NotImplementedError

    def floor(self):
        return DualNumber(math.floor(self.val), 0)

    def ceil(self):
        return DualNumber(math.ceil(self.val), 0)

    def fsum(u):
        return DualNumber(math.fsum(map(V, u)), math.fsum(map(D, u)))

    def fabs(u):
        return abs(DualNumber(u))

    def fmod(u, v):
        return DualNumber(u) % v

    def trunc(self):
        return DualNumber(math.trunc(self.val), 0)

    def copysign(self, v):
        return DualNumber(
            math.copysign(self.val, V(v)),
            self.der if math.copysign(1, self.val * V(v)) > 0 else -self.der,
        )

    def __repr__(self):
        return f"{self.val}, {self.der}"


def V(x):
    return getattr(x, "val", x)


def D(x):
    return getattr(x, "der", 0)
