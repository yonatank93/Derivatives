import numpy as np
import copy

from Derivatives.finite_diff import finite_diff
from Derivatives.richardson import richardson_deriv
from Derivatives.chebyshev import Chebyshev
from Derivatives.auto_diff import DualNumber

finite_diff_methods = ["central", "forward", "backward", "5-point"]
other_methods = ["richardson", "chebishev", "auto_diff"]
avail_methods = np.append(finite_diff_methods, other_methods)


def derivatives(f, x0, method="central", **kwargs):
    """Estimate the derivative of function f(x) at x0.

    Parameters
    ----------
    f: callable
        Function to calculate the derivative of.
    x0: float
        Point to evaluate the derivative
    method: str
        Method to use to approximate the finite difference derivative. Options
        available: "central", "forward", "backward", "5-point",
        "richardson_deriv", "chebishev", "auto_diff".
    **kwargs: dict
        Keyword arguments for additional parameters the derivative functions
        take.

    Returns
    -------
    floaf
        Estimation of the derivative of f(x) at x0.

    Raises
    ------
    ValueError
        When the method is not found.
    """
    if method in finite_diff_methods:
        df = finite_diff(f, x0, method, **kwargs)
    elif method == "richardson":
        df = richardson_deriv(f, x0, **kwargs)
    elif method == "chebyshev":
        cheb = Chebyshev(f, kwargs["a"], kwargs["b"])
        kwargs.pop("a")
        kwargs.pop("b")
        cheb.fit(**kwargs)
        cheb.derivative()
        df = cheb.evaluate(x0)
    elif method == "auto_diff":
        ndim = np.ndim(x0)
        x0 = np.asarray([x0]).flatten()
        df = np.asarray([f(DualNumber(x, 1)).der for x in x0])
        if not ndim:
            df = df[0]
    else:
        raise ValueError(
            f"{method} method not found. \nAvailable methods: {avail_methods}"
        )

    return df


def jacobian(model, x0, method, npred, **kwargs):
    """Estimate the jacobian of a model at x0.

    Parameters
    ----------
    model: callable
        Model to calculate the jacobian of.
    x0: float
        Point to evaluate the jacobian.
    method: str
        Method to use to approximate the finite difference derivative. Options
        available: "central", "forward", "backward", "5-point",
        "richardson_deriv", "chebishev", "auto_diff".
    **kwargs: dict
        Keyword arguments for additional parameters the derivative functions
        take.

    Returns
    -------
    floaf
        Estimation of the derivative of f(x) at x0.

    Raises
    ------
    ValueError
        When the method is not found.
    """
    nparams = len(x0)
    J = np.empty((npred, nparams))  # Matrix to store the Jacobian
    kwargs.update({"nout": npred})

    for ii, xx in enumerate(x0):
        # For each iteration, compute the derivative of the model wrt. the
        # i^th parameter
        params = copy.copy(x0)

        def model_wrapper(x):
            params[ii] = x
            return model(params)

        J[:, ii] = derivatives(model_wrapper, xx, method, **kwargs)
    return J
