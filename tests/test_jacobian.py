import numpy as np
import os
from Derivatives import jacobian
from data import exponential_model


file_dir = os.path.dirname(os.path.realpath(__file__))
data_file = "jacobian_exp_model.npz"

load_data = np.load(file_dir + "/" + data_file)
J = load_data["J"]
t = load_data["t"]
p = load_data["p"]

model = exponential_model(t).predictions
J2 = jacobian(model, p, "richardson", len(t), t=2.1)

shape = J2.shape


def test_shape():
    """Test if the implementation of the jacobian of a model gives a matrix with
    the correct size. This is done by comparing the number of rows and columns
    of the jacobian matrix to the numbers of time stamp and parameters. The
    number of rows should be equal to the number of time stamp (or the number of
    predictions the model makes) and the number of columns should be equal to
    the number of parameters.
    """
    assert shape[0] == len(t)
    assert shape[1] == len(p)


def test_values():
    """Test if the implementation of the jacobian of a model gives the correct
    values. This is done by comparing the jacobian computed using the
    implementation here with the jacobian computed using some trusted package.
    """
    assert np.allclose(J, J2)
