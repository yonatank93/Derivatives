import time
import pprint
from Derivatives import derivatives
import data


comparison_dict = {}

kwargs = {
    "central": {"h": 0.01},
    "forward": {"h": 1e-3},
    "backward": {"h": 1e-3},
    "5-point": {"h": 0.01},
    "richardson": {"niter": 10},
    "chebyshev": {"a": data.lb, "b": data.ub},
}

for method in kwargs:
    start = time.perf_counter()
    df = derivatives(data.f, data.x_list, method=method, **kwargs[method])
    comparison_dict.update(
        {
            method: {
                "time": time.perf_counter() - start,
                "error": data.error(data.df_list, df),
            }
        }
    )

print("Comparison")
pprint.pprint(comparison_dict)
