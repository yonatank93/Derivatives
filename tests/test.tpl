import numpy as np
import data

# Import this to test the implementation of the method, before it is added
# to derivative.py
from method_name import method_name

# Import this after the method is added to derivative.py
from derivatives import derivatives


def test_derivative_method_name():
    """Check if the {method's name} is implemented correctly. This is done by
    estimating the derivative of the sine function at 100 points between 0 and
    2 pi using the {method's name} implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """

    # Before the method is added to derivative.py, use the following command
    df = method_name(data.f, data.x_list, **kwargs)

    # After the derivative.py is updated, use the following command
    df = derivatives(data.f, data.x_list, method=method_name, **kwargs)

    # Assert the results
    assert np.all(np.isclose(df, data.df_list, rtol=1e-5))
