import numpy as np

"""Use f(x) to be a simple sine function, thus f'(x) is a cosine function"""


def f(x):
    """Function to use in the testing process."""
    return np.sin(x)


def df(x):
    """Derivative of the function to use in the testing process."""
    return np.cos(x)


def error(data, estimate):
    """Compute sum of the the absolute error."""
    return np.sum(np.abs(data - estimate))


lb = 0
ub = 2 * np.pi
x_list = np.random.uniform(lb, ub, 100)
df_list = df(x_list)
tol = 1e-3


# Exponential model with 2 parameters and 3 predictions
class exponential_model:
    """y(theta, t) = exp(-exp(theta0)*t) + exp(-exp(theta1)*t) + ...
    The choice is made so that the parameter we have decaying function for all
    theta.
    """

    def __init__(self, t):
        self.t = np.array(t)  # timestep data
        self.npred = len(t)

    def __basis(self, theta):
        return np.exp(-theta * self.t)

    def __basis_J(self, theta):
        return -self.t * np.exp(-theta * self.t)

    def predictions(self, p):
        return np.sum(np.array(list(map(self.__basis, p))), axis=0)
