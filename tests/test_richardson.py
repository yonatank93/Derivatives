import numpy as np
import pytest
from Derivatives import richardson_deriv
import data


def test_richardson():
    """Check if richardson extrapolation to the central difference is
    implemented correctly. This is done by estimating the derivative of the
    sine function at 100 points between 0 and 2 pi using the richardson
    derivative implementation. Then, the results are asserted to be
    approximately equal to the known analytic derivative of the function.
    """
    df = richardson_deriv(data.f, data.x_list, niter=10)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_richardson_deriv_exceptions():
    """Test if the ValueError exception is raised in richardson_deriv. This is
    done by computing the derivative of a function at random points stored in a
    2-dimensional ndarray.
    """
    with pytest.raises(ValueError):
        richardson_deriv(data.f, np.random.uniform(0, np.pi, (5, 5)))
