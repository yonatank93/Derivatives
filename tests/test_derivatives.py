import numpy as np
import pytest
from Derivatives import derivatives
import data


def test_central_diff():
    """Check if the central difference is implemented correctly via derivatives
    interface. This is done by estimating the derivative of the sine function
    at 100 points between 0 and 2 pi using the central difference
    implementation. Then, the results are asserted to be approximately equal to
    the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="central", h=0.01)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_forward_diff():
    """Check if the forward difference is implemented correctly via derivatives
    interface. This is done by estimating the derivative of the sine function
    at 100 points between 0 and 2 pi using the forward difference
    implementation. Then, the results are asserted to be approximately equal to
    the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="forward", h=1e-3)
    assert np.all(np.isclose(df, data.df_list, atol=data.tol))


def test_backward_diff():
    """Check if the backward difference is implemented correctly via derivatives
    interface. This is done by estimating the derivative of the sine function
    at 100 points between 0 and 2 pi using the backward difference
    implementation. Then, the results are asserted to be approximately equal to
    the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="backward", h=1e-3)
    assert np.all(np.isclose(df, data.df_list, atol=data.tol))


def test_5_point_diff():
    """Check if the 5-point difference is implemented correctly via derivatives
    interface. This is done by estimating the derivative of the sine function
    at 100 points between 0 and 2 pi using the 5-point difference
    implementation. Then, the results are asserted to be approximately equal to
    the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="5-point", h=0.01)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_richardson():
    """Check if richardson extrapolation to the central difference is
    implemented correctly via derivatives interface. This is done by estimating
    the derivative of the sine function at 100 points between 0 and 2 pi using
    the richardson derivative implementation. Then, the results are asserted to
    be approximately equal to the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="richardson", niter=10)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_chebyshev():
    """Check if Chebyshev derivative is implemented correctly via derivatives
    interface. This is done by estimating the derivative of sine function at
    100 points between 0 and 2 pi using the Chebyshev derivative
    implementation. Then, the results are asserted to be approximately equal to
    the known analytic derivative of the function.
    """
    df = derivatives(
        data.f, data.x_list, method="chebyshev", a=data.lb, b=data.ub
    )
    assert np.all(np.isclose(df, data.df_list))


def test_auto_diff():
    """Check if automatic differentiation is implemented correctly via
    derivatives interface. This is done by estimating the derivative of sine
    function at 100 points between 0 and 2 pi using the automatic
    differentiation implementation. Then, the results are asserted to be
    approximately equal to the known analytic derivative of the function.
    """
    df = derivatives(data.f, data.x_list, method="auto_diff")
    assert np.all(np.isclose(df, data.df_list))


def test_derivatives_exceptions():
    """Test if the ValueError exception is raised when the method requested is
    not found in derivatives.py. This is done by computing the derivative of a
    function and request to use the 3-point method, which is not going to be
    implemented.
    """
    with pytest.raises(ValueError):
        derivatives(data.f, data.x_list, method="3-point")
