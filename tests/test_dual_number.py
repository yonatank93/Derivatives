import math
import numpy as np
import scipy.special
import pytest
from Derivatives import DualNumber

U, V, DU, DV = 1.2, 0.3, 2, -0.5
y1 = DualNumber(U, DU)
y2 = DualNumber(V, DV)


def compare(dual, target_val, target_der):
    """Base function to do the comparison. `np.isclose` is used to prevent
    error due to rounding.
    """
    assert np.isclose(dual.val, target_val)
    assert np.isclose(dual.der, target_der)


def test_add():
    compare(y1 + y2, U + V, DU + DV)


def test_subtract():
    compare(y1 - y2, U - V, DU - DV)


def test_multiply():
    compare(y1 * y2, U * V, DU * V + U * DV)


def test_true_division():
    compare(y1 / y2, U / V, (DU * V - U * DV) / V ** 2)


def test_power():
    compare(y1 ** y2, U ** V, U ** (V - 1) * (DU * V + np.log(U) * U * DV))


def test_floor_division():
    compare(y1 // y2, U // V, 0)


def test_mod():
    compare(y1 % y2, U % V, DU - U // V * DV)


def test_positive():
    compare(+y1, U, DU)


def test_negative():
    compare(-y1, -U, -DU)


def test_radd():
    target = DualNumber(U) + y2
    compare(U + y2, target.val, target.der)


def test_rmul():
    target = DualNumber(U) * y2
    compare(U * y2, target.val, target.der)


def test_rsub():
    target = DualNumber(U) - y2
    compare(U - y2, target.val, target.der)


def test_rtruediv():
    target = DualNumber(U) / y2
    compare(U / y2, target.val, target.der)


def test_rpow():
    target = DualNumber(U) ** y2
    compare(U ** y2, target.val, target.der)


def test_rmod():
    target = DualNumber(U) % y2
    compare(U % y2, target.val, target.der)


def test_rfloordiv():
    target = DualNumber(U) // y2
    compare(U // y2, target.val, target.der)


def test_abs():
    compare(abs(y2), abs(V), abs(DV))


def test_sqrt():
    compare(np.sqrt(y1), np.sqrt(U), DU / (2 * np.sqrt(U)))


def test_log():
    compare(np.log(y1), np.log(U), DU / U)


def test_log2():
    compare(np.log2(y1), np.log2(U), DU / (U * np.log(2)))


def test_log10():
    compare(np.log10(y1), np.log10(U), DU / (U * np.log(10)))


def test_log1p():
    compare(np.log1p(y1), np.log1p(U), DU / (1 + U))


def test_exp():
    exp = np.exp(U)
    compare(np.exp(y1), exp, DU * exp)


def test_expm1():
    compare(np.expm1(y1), np.expm1(U), DU * np.exp(U))


def test_sin():
    compare(np.sin(y1), np.sin(U), DU * np.cos(U))


def test_cos():
    compare(np.cos(y1), np.cos(U), -DU * np.sin(U))


def test_tan():
    compare(np.tan(y1), np.tan(U), DU / np.cos(U) ** 2)


def test_arcsin():
    compare(np.arcsin(y2), np.arcsin(V), DV / np.sqrt(1 - V ** 2))


def test_arccos():
    compare(np.arccos(y2), np.arccos(V), -DV / np.sqrt(1 - V ** 2))


def test_arctan():
    compare(np.arctan(y2), np.arctan(V), DV / (1 + V ** 2))


def test_sinh():
    compare(np.sinh(y1), np.sinh(U), DU * np.cosh(U))


def test_cosh():
    compare(np.cosh(y1), np.cosh(U), DU * np.sinh(U))


def test_tanh():
    compare(np.tanh(y1), np.tanh(U), DU / np.cosh(U) ** 2)


def test_arcsinh():
    compare(np.arcsinh(y1), np.arcsinh(U), DU / (np.sqrt(U ** 2 + 1)))


def test_arccosh():
    compare(np.arccosh(y1), np.arccosh(U), DU / np.sqrt(U ** 2 - 1))


def test_arctanh():
    compare(np.arctanh(y2), np.arctanh(V), DV / (1 - V ** 2))


def test_radians():
    compare(np.radians(y1), np.radians(U), np.radians(DU))


def test_degrees():
    compare(np.degrees(y1), np.degrees(U), np.degrees(DU))


def test_hypot():
    dist = np.hypot([U], [V])
    compare(np.hypot(y1, y2), dist, (U * DU + V * DV) / dist)


def test_fsum():
    compare(np.sum([y1, y2]), U + V, DU + DV)
