import numpy as np
import pytest
from Derivatives import finite_diff
import data


def test_central_diff():
    """Check if the central difference is implemented correctly. This is done by
    estimating the derivative of the sine function at 100 points between 0 and
    2 pi using the central difference implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """
    df = finite_diff(data.f, data.x_list, method="central", h=0.01)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_forward_diff():
    """Check if the forward difference is implemented correctly. This is done by
    estimating the derivative of the sine function at 100 points between 0 and
    2 pi using the forward difference implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """
    df = finite_diff(data.f, data.x_list, method="forward", h=1e-3)
    assert np.all(np.isclose(df, data.df_list, atol=data.tol))


def test_backward_diff():
    """Check if the backward difference is implemented correctly. This is done by
    estimating the derivative of the sine function at 100 points between 0 and
    2 pi using the backward difference implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """
    df = finite_diff(data.f, data.x_list, method="backward", h=1e-3)
    assert np.all(np.isclose(df, data.df_list, atol=data.tol))


def test_5_point_diff():
    """Check if the 5-point difference is implemented correctly. This is done by
    estimating the derivative of the sine function at 100 points between 0 and
    2 pi using the 5-point difference implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """
    df = finite_diff(data.f, data.x_list, method="5-point", h=0.01)
    assert np.all(np.isclose(df, data.df_list, rtol=data.tol))


def test_finite_diff_exceptions():
    """Test if the ValueError exception is raised when the method requested is
    not found in finite_diff.py. This is done by computing the derivative of a
    function and request to use the 3-point method, which is not going to be
    implemented.
    """
    with pytest.raises(ValueError):
        finite_diff(data.f, data.x_list, method="3-point")
