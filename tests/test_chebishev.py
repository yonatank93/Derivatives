import numpy as np
import pytest
import data
from Derivatives.chebyshev import Chebyshev


cheb = Chebyshev(data.f, data.lb, data.ub)
cheb.fit()


def test_fit():
    """Check if Chebishev series expansion is implemented correctly. This is
    done by estimating the sine function at 100 points between 0 and 2 pi using
    the Chebishev series implementation. Then, the results are asserted to be
    approximately equal to the known analytic function.
    """
    assert np.all(np.isclose(cheb.evaluate(data.x_list), data.f(data.x_list)))


def test_derivative():
    """Check if Chebishev derivative is implemented correctly. This is done by
    estimating the derivative of sine function at 100 points between 0 and 2 pi
    using the Chebishev derivative implementation. Then, the results are
    asserted to be approximately equal to the known analytic derivative of the
    function.
    """
    cheb.derivative()
    assert np.all(np.isclose(cheb.evaluate(data.x_list), data.df(data.x_list)))


def test_integral():
    """Check if Chebishev integral is implemented correctly. This is done by
    estimating the integral of sine function at 100 points between 0 and 2 pi
    using the Chebishev integral implementation. Then, the results are
    asserted to be approximately equal to the known analytic integral of the
    function.
    """
    cheb.integral()
    assert np.all(np.isclose(cheb.evaluate(data.x_list), data.f(data.x_list)))


def test_chebishev_exceptions():
    """Test if the ValueError exception is raised in Chebishev.evaluate. This
    is done by evaluating the series at x=-1, which is outside the domain used
    to fit the coefficients.
    """
    with pytest.raises(ValueError):
        cheb.evaluate(-1)
