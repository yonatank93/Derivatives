import numpy as np
from Derivatives.finite_diff import finite_diff

__all__ = ["richardson_deriv", "richardson"]


def richardson_deriv(
    f, x0, nout=None, h=0.1, t=2, niter: int = 8, tol=1e-8, full_output=False
):
    """Implementation of Richardson's extrapolation to finite difference method.

    Parameters
    ----------
    f: callable
        Function to calculate the derivative of.
    x0: {float, int, ndarray}
        Point to evaluate the derivative.
    nout: int
        Number of output of the function f.
    h: float, optional
        Initial stepsize.
    t: float, optional
        Inverse scalling on the stepsize.
    niter: int, optional
        Number of iteration.
    tol: float, optional
        Tolerance of the derivative value.
    full_output: bool, optional
        Option to return other information.

    Returns
    -------
    float
        Approximation to first derivative of f(x).
    dict
        If full_output=True, return final result, full derivative matrix,
        number of iterations, and error.

    Raises
    ------
    ValueError
        If the output of the function has dimension larger than 1 (vector).
    """
    func = _derivative_wrapper(f, x0, h)
    ndim = np.ndim(x0)
    if nout is None:
        if isinstance(x0, int):
            nout = 1
        else:
            nout = len(x0)
    # if ndim == 0:
    #     nout = 1
    # elif ndim == 1:
    #     nout = len(x0)
    if ndim > 1:
        raise ValueError(
            f"Cannot do computation to function with {ndim} dimensional output"
        )
    return richardson(
        func,
        noutput=nout,
        h=h,
        t=t,
        niter=niter,
        tol=tol,
        full_output=full_output,
    )


def richardson(
    f,
    noutput: int,
    h=0.1,
    args=(),
    t=2,
    niter: int = 8,
    tol=1e-8,
    full_output=False,
):
    """Implementation of Richardson's extrapolation to any function.

    Parameters
    ----------
    f: callable
        Function to estimate. This function takes the step size h as the first
        argument, and returns a number or array.
    noutput: int
        Number of output of the function f.
    h: float, optional
        Initial stepsize.
    args: tuple, optional
        Extra arguments passed to the function.
    t: float, optional
        Inverse scalling of the stepsize.
    niter: int, optional
        Number of iteration.
    tol: float, optional
        Tolerance of the derivative value.
    full_output: bool, optional
        Option to return other information.

    Returns
    -------
    float
        Approximation :math:`\\lim_{h \\to 0} f(h)`.
    dict
        If full_output=True, return the final result, full matrix, number of
        iterations, and error.
    """
    if noutput == 1:
        np.zeros((niter + 1, niter + 1))
    elif noutput > 1:
        AA = np.zeros((niter + 1, niter + 1, noutput))
    AA[0, 0] = f(h, *args)  # Initial value of the derivative

    # Iterative improvement using Richardson's extrapolation
    for ii in range(1, niter + 1):
        h /= t
        AA[ii, 0] = f(h, *args)

        for jj in range(ii):
            AA[ii, jj + 1] = AA[ii, jj] + (AA[ii, jj] - AA[ii - 1, jj]) / (
                4 ** jj
            )

        error = np.abs(AA[ii, ii] - AA[ii - 1, ii - 1])
        if np.max(error) < tol:
            AA = AA[: ii + 1, : ii + 1]
            break

    if full_output:
        toreturn = {
            "df": AA[-1, 1],
            "deriv_matrix": AA,
            "iterations": ii,
            "error": error,
        }
    else:
        toreturn = AA[-1, -1]
    return toreturn


def _derivative_wrapper(f, x0, h):
    """Wrapper to the derivative function."""

    def wrapper(h):
        return finite_diff(f, x0, h=h)

    return wrapper
