import numpy as np


class Chebyshev:
    """Class for Chebishev approximation and related methods


    Parameters
    ----------
    func: callable
            Function to approximate.
    a, b: float
        Lower and upper bound of the region to approximate.
    """

    def __init__(self, func, a, b):
        self.func = func
        self.a = a
        self.b = b
        self._coef = None

    def fit(self, n: int = 20):
        """Approximate the function func in the interval [a, b] with n terms.

        Parameters
        ----------
        n: int
            Number of terms in the series.
        """
        self.n = n
        self.m = n

        # The following constants are used to transform the domain
        bma = (self.b - self.a) / 2
        bpa = (self.b + self.a) / 2

        # Using Eq. 5.8.4 and 5.8.10
        f = np.empty(n)
        for kk in range(n):
            y = np.cos(np.pi * (kk + 0.5) / n)  # Eq. 5.8.4
            f[kk] = self.func(y * bma + bpa)  # Eq. 5.8.10

        fac = 2 / n
        self._coef = np.empty(n)
        for jj in range(n):
            s = np.sum(f * np.cos(np.pi * jj * (np.arange(n) + 0.5) / n))
            self._coef[jj] = fac * s  # Eq. 5.8.8

    @property
    def coef(self):
        """Coefficients of the series expansion."""
        return self._coef

    @coef.setter
    def coef(self, const):
        """Costructor from previously computed coefficients.

        Parameters
        ----------
        const: ndarray
            Array of coefficients of the expansion.
        """
        self._coef = const
        self.n = len(const)

    def set_m(self, threshold):
        """Set m, the number of coefficients after truncating to an error level
        threshold, and return the value set.

        Parameters
        ----------
        threshold: float
            Error threshold.
        """
        while self.m > 1 and abs(self.coef[self.m - 1]) < threshold:
            self.m -= 1

    def evaluate(self, x, m: int = None):
        """Chebishev evaluation. The Chebishev function is evaluated at a point
        y, and the result is retrned as the function value.

        Parameters
        ----------
        x: float
            Point to evaluate.
        m: int
            Number of coefficient.

        Returns
        -------
        val: float
            Chebishev series expansion of the function evaluated at x.
        """
        if not m:
            m = self.m
        else:
            assert m <= self.n
        d, dd = [0.0, 0.0]

        ndim = np.ndim(x)
        in_range = (x - self.a) * (x - self.b) <= 0
        if ndim > 0:
            in_range = np.all(in_range)
        if not in_range:
            raise ValueError("x is not in the range of Chebishev evaluation")

        y = (2 * x - self.a - self.b) / (self.b - self.a)  # Eq.5.8.10

        # use eq. 5.8.11 to evaluate the function
        for jj in np.arange(self.m - 1, 0, -1):
            sv = d
            d = 2 * y * d - dd + self.coef[jj]
            dd = sv

        return y * d - dd + self.coef[0] / 2

    def derivative(self):
        """Return a new Chebishev object that approximates the derivative of
        the existing function over the same range [a, b].
        """
        coef_der = np.empty(self.n)
        # The last 2 coefficients are special cases
        coef_der[-2:] = [2 * (self.n - 1) * self.coef[-1], 0]

        for jj in np.arange(self.n - 2, 0, -1):
            # Apply Eq. 5.9.2
            coef_der[jj - 1] = coef_der[jj + 1] + 2 * jj * self.coef[jj]

        # Normalize to the intervale b-a
        con = 2 / (self.b - self.a)
        coef_der *= con

        # Set new coefficients in the Chebishev series
        self.coef = coef_der

    def integral(self):
        """Return a new Chebyshev object that approximates the indefinite
        integral of the existing function over the same range [a,b]. The
        constant of integration is set so that the integral vanishes at a.
        """
        ss = 0
        fac = 1
        coef_int = np.empty(self.n)

        # Factor that normalizes to the interval b-a
        con = (self.b - self.a) / 4
        for jj in range(1, self.n - 1):
            # Apply eq. 5.9.1
            coef_int[jj] = con * (self.coef[jj - 1] - self.coef[jj + 1]) / jj
            ss += fac * coef_int[jj]  # Accumulates the constant of integration
            fac = -fac  # This will only be +-1

        # Special case for the last constant in eq. 5.9.1
        coef_int[-1] = con * self.coef[-2] / (self.n - 1)

        # Set the constant of integration
        ss += fac * coef_int[-1]
        coef_int[0] = 2 * ss

        # Set new coefficients in the Chebishev series
        self.coef = coef_int

    def polycof(self, m: int):
        """Polynomial coefficients from a Chebyshev fit. Given a coefficient
        array c[0..n-1], this routine returns a coefficient array d[0..n-1].
        The method is Clenshaw’s recurrence (5.8.11), but now applied
        algebraically rather than arithmetically.
        """

        d = np.zeros(m)
        dd = np.zeros(m)

        d[0] = self.coef[m - 1]
        for jj in np.arange(m - 2, 0, -1):
            for kk in np.arange(m - jj, 0, -1):
                sv = d[kk]
                d[kk] = 2 * d[kk - 1] - dd[kk]
                dd[kk] = sv

            sv = d[0]
            d[0] = -d[0] + self.coef[jj]
            dd[0] = sv

        for jj in np.arange(m - 1, 0, -1):
            d[jj] = d[jj - 1] - dd[jj]

        d[0] = -dd[0] + self.coef[0] / 2

        return d
