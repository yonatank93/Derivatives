__all__ = ["finite_diff"]

avail_method = ["central", "forward", "backward", "5-point"]


def finite_diff(f, x0, method="central", h=0.1):
    """Approximation to the derivative of f(x) at x0 using finite difference
    method.

    Parameters
    ----------
    f: callable
        Function to calculate the derivative of.
    x0: float
        Point to evaluate the derivative.
    method: str
        Method to use to approximate the finite difference derivative. Options
        available: "central", "forward", "backward", "5-point".
    h: float, optional
        Step size to use.

    Returns
    -------
    floaf
        Estimation of the derivative of f(x) at x0.

    Raises
    ------
    ValueError
        If the method is not in ["central", "forward", "backward", "5-point"]
    """
    if method == "central":
        df = _central_diff(f, x0, h)
    elif method == "forward":
        df = _forward_diff(f, x0, h)
    elif method == "backward":
        df = _backward_diff(f, x0, h)
    elif method == "5-point":
        df = _five_point(f, x0, h)
    else:
        raise ValueError(f"Available methods: {avail_method}")
    return df


def _central_diff(f, x0, h):
    """Approximation to the derivative of f(x) at x0 using central difference
    method.
    """
    return (f(x0 + h) - f(x0 - h)) / (2 * h)


def _forward_diff(f, x0, h):
    """Approximation to the derivative of f(x) at x0 using forward difference
    method.
    """
    return (f(x0 + h) - f(x0)) / h


def _backward_diff(f, x0, h):
    """Approximation to the derivative of f(x) at x0 using backward difference
    method.
    """
    return (f(x0) - f(x0 - h)) / h


def _five_point(f, x0, h):
    """Approximation to the derivative of f(x) at x0 using backward difference
    method.
    """
    numerator = f(x0 - 2 * h) - 8 * f(x0 - h) + 8 * f(x0 + h) - f(x0 + 2 * h)
    denominator = 12 * h
    return numerator / denominator
