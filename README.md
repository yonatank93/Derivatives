# derivatives

Implementation of some numerical (first) derivatives methods.

## Get started
Make sure `derivatives` is in the python load path. To add the path,
```
# Linux & OSX
export PYTHONPATH=/path/to/derivatives/:$PYTHONPATH

# Windows
set PYTHONPATH=C:\path\to\derivatives\;%PYTHONPATH%
```

Ready to use `derivatives`:
```bash
$ pip install -r requirements.txt  # Install required packages
$ python -m pytest .  # Run the test
```

## Content
The numerical derivative methods implementer here:

* Finite difference (central, forward, backward, 5-point)
* Richardson's extrapolation to central difference
* Automatic differentiation
* Chebishev method

## Example
Suppose we want to approximate the derivative of sine function at $`x=1`$.
```python
import numpy as np
from Derivatives import derivatives

df_f = derivatives(np.sin, 1, method="central", h=1e-2)  # Using central difference
df_r = derivatives(np.sin, 1, method="richardson", niter=10)  # Using richardson's extrapolation
df_c = derivatives(np.sin, 1, method="chebyshev", a=-1, b=1)  # Using Chebyshev derivative
```
